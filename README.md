# Straffeloven 2005 - se [lovdata](https://lovdata.no/dokument/NL/lov/2005-05-20-28?q=straffeloven%202005)
* Loven kan anvendes f.eks ved:
    * Innbrudd i datasystemer (§ 204)
        * Med bot eller fengsel inntil 2 år straffes den som ved å bryte en beskyttelse eller ved annen uberettiget fremgangsmåte skaffer seg tilgang til datasystem eller del av det.
    * Egne ansatte røper bedriftshemmeligheter (§ 209 - 211)
        * § 209. Brudd på taushetsplikt
            * Med bot eller fengsel inntil 1 år straffes den som røper opplysning som han har taushetsplikt om i henhold til lovbestemmelse eller forskrift, eller utnytter en slik opplysning med forsett om å skaffe seg eller andre en uberettiget vinning.
            * Første ledd gjelder tilsvarende ved brudd på taushetsplikt som følger av gyldig instruks for tjeneste eller arbeid for statlig eller kommunalt organ.
            * For den som arbeider eller utfører tjeneste for et statlig eller kommunalt organ, rammer første og annet ledd også brudd på taushetsplikt etter at tjenesten eller arbeidet er avsluttet.
            * Grovt uaktsom overtredelse straffes på samme måte.
            * Medvirkning er ikke straffbar.
                * 0	Tilføyd ved lov 19 juni 2009 nr. 74.
        * § 210. Grovt brudd på taushetsplikt
            * Grovt brudd på taushetsplikt straffes med fengsel inntil 3 år.
            * Ved avgjørelsen av om taushetsbruddet er grovt skal det særlig legges vekt på om gjerningspersonen har hatt forsett om uberettiget vinning og om handlingen har ført til tap eller fare for tap for noen.
                * 0	Tilføyd ved lov 19 juni 2009 nr. 74.
        * § 211. Brudd på taushetsplikt for enkelte yrkesgrupper
            * Med bot eller fengsel inntil 1 år straffes prester i Den norske kirke, prester eller forstandere i registrerte trossamfunn, advokater, forsvarere i straffesaker, meklingsmenn i ekteskapssaker, og disses hjelpere, som uberettiget røper hemmeligheter som er betrodd dem eller deres foresatte i anledning av stillingen eller oppdraget.
                * 0	Tilføyd ved lov 19 juni 2009 nr. 74, endret ved lov 15 juni 2018 nr. 37 (ikr. 1 juli 2018 iflg. res. 15 juni 2018 nr. 887).
    * Logiske bomber eller datavirus som medfører skade (§ 351)
        * § 351. Skadeverk
            * Med bot eller fengsel inntil 1 år straffes den som skader, ødelegger, gjør ubrukelig eller forspiller en gjenstand som helt eller delvis tilhører en annen.
            * For skadeverk straffes også den som uberettiget endrer, gjør tilføyelser til, ødelegger, sletter eller skjuler andres data.
                * 0	Tilføyd ved lov 19 juni 2009 nr. 74.
    * Forfalskning, også ved hjelp av datamaskiner (§ 371)
        * § 371. Bedrageri
            * Med bot eller fengsel inntil 2 år straffes den som med forsett om å skaffe seg eller andre en uberettiget vinning
            * a)	fremkaller, styrker eller utnytter en villfarelse og derved rettsstridig forleder noen til å gjøre eller unnlate noe som volder tap eller fare for tap for noen, eller
            * b)	bruker uriktig eller ufullstendig opplysning, endrer data eller datasystem, disponerer over et kredittkort eller debetkort som tilhører en annen, eller på annen måte uberettiget påvirker resultatet av en automatisert databehandling, og derved volder tap eller fare for tap for noen.
                * 0	Tilføyd ved lov 19 juni 2009 nr. 74.
        * § 372. Grovt bedrageri
            * Grovt bedrageri straffes med fengsel inntil 6 år. Ved avgjørelsen av om bedrageriet er grovt skal det særlig legges vekt på om
            * a)	det har hatt til følge en betydelig økonomisk skade,
            * b)	det er voldt velferdstap eller fare for liv eller helse,
            * c)	det er begått ved flere anledninger eller over lengre tid,
            * d)	det er begått av flere i fellesskap eller har et systematisk eller organisert preg,
            * e)	lovbryteren har foregitt eller misbrukt stilling, verv eller oppdrag,
            * f)	det er ført eller utarbeidet uriktige regnskaper eller uriktig regnskapsdokumentasjon, eller
            * g)	lovbryteren har forledet allmennheten eller en større krets av personer.
                * 0	Tilføyd ved lov 19 juni 2009 nr. 74.
        * § 373. Mindre bedrageri
            * Bedrageri straffes med bot når straffskylden er liten fordi det gjaldt en ubetydelig verdi og forholdene for øvrig tilsier det.
                * 0	Tilføyd ved lov 19 juni 2009 nr. 74.